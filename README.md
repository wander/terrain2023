## BGTTerrain (HDRP 2023) (UNDER CONSTRUCTION)

Builds up a terrain using PDOK BGT information.

### Dependencies (GIT Pacakages)
- terrain
- vectortile
- geotiff
- utils

### Usage:

Sweep the TerrainBuilder2023 prefab into the Hierarchy and press 'Build' on the TerrainBuilder script.


### Note:

This package adds resources that are compatible with Unity 2023 (HDRP). Scripts are maintained in the (original)
terrain package.